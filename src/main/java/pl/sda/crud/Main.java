package pl.sda.crud;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static final String QUIT = "q";
    public static final String ADD_CAR_OPTION = "a";
    public static final String UPDATE_CAR_OPTION = "b";
    public static final String DELETE_CAR_OPTION = "c";
    public static final String GET_BY_ID_OPTION = "d";
    public static final String GET_ALL_OPTION = "e";

    /**
     * H1. Utworz klase Car majaca pola id (long) brand (enum), model (string), price (bigdecimal),
     * horsepower (int), nie zapomnij o getterach i konstruktorze (moga byc tez settery jesli chcesz)
     * H2. Utworz klase CarDatabase, powinna miec pole List<Car>, zdefiniuj metody:
     * add(Car) -> dodaje elementy do listy
     * update(Car) -> powinno znalezc w liscie samochod o id takim jak w argumencie i zaktualizowac pozostale pola
     * get(id) -> powinno zwrocic auto o identyfikatorze id
     * delete(id) -> powinno usunac auto o identyfikatorze id
     * getAll() -> powinno zwrocic zawartosc listy
     * H3. Pozwol uzytkownikowi na korzystanie z powyzszych metod uzywajac konsoli, wypisywanie powinno byc zrealizowane w nastepujacy sposob:
     * id |  marka  | model |   cena | ilosc koni |
     * 1 |   AUDI  |    Q4 | 220000 |        300 |
     * 2 |PORSCHE   |   911 | 520000 |        320 |
     * 4. Dodaj do klasy CarDatabase pole DATEBASE_LOCATION (Path) i metode save(), read() zapisujace liste do pliku w formcie CSV, np.
     * 1;AUDI;Q4;220000;300
     * 2;PORSCHE;911;520000;320
     * 5. Dodaj do klasy Car pole quantity (long) oznaczajace ilosc samochodow, ktore pozostaly w salonie (pamietaj o konstruktorze, getterze, ewentualnie setterach i zapisie do pliku)
     * 6. Dodaj klase CarSeller z polami:
     * - cash (BigDecimal) oznaczajacym ilosc pieniedzy pozostalych na koncie salonu
     * - CarDatabase pozwalajaca na manipulacje danymi zawartymi w bazie danych
     * i metodami:
     * - sell(id) powinna zmniejszac ilosc samochodow i zwiekszac ilosc pieniedzy
     * - buy(Car) odejmowac pieniadze i dodawac samochod do bazy (jesli istnieje to zwiekszac jego licznosc)
     * 7. Zmien interfejs uzytownika dostepny w konsoli na taki, ktory obsluguje metody z CarSeller (a wiec teraz juz tylko obslugujemy metody buy i sell)
     * 8. Dodaj logowanie, uzytkownik dopiero po wprowadzeniu odpowiedniego hasla i loginu powinien moc miec dostep do interfejsu uzytkownika
     * "na sztywno" zaszyj w aplikacji haslo i login, przed przejsciem do wyswietlania menu pytaj o login i haslo
     * 9. Zamiast trzymac na sztywno w aplikacji login i haslo przenies je do pliku properties
     * 10. Uzytkownikow aplikacji moze byc wielu, dlatego utworz klase UserDatabase, ktora bedzie odpowiedzialna za zapis, odczyt, usuwanie, aktualizowanie uzytkownikow
     * - metody analogiczne jak w CarDatabase (nie zapomnij o save i read
     * - dodatkowo napisz metode findByLogin, ktorej uzyjesz w nastepnym punkcie (powinna zwracac uzytkownika po loginie)
     * 11. Zeby uzytkownik mogl sie zalogowac od teraz korzystaj z UserDatabase (pobieraj uzytkownika po loginie)
     * 12. *Zahashuj haslo
     * 13. Dodaj klase SoldCar z polem String model i String login
     * 14. Dodaj klase SoldCarsDatabase (analogiczne metody jak w innych *Database)
     * 15. Dodaj klase SoldCarReporter z metodami:
     * reportSale(String login, Car car) korzystajaca z SoldCarsDatabase
     * getSales() zwracajaca wszystkie sprzedaze zawarte w bazie
     * findMostValuableSeller() zwracajaca login pracownika, ktory dokonal najwiekszej liczby sprzedazy
     */
    public static void main(String[] args) {
        CarDatabase database = new CarDatabase();
        //System.out.println("| id | Kolumna 1 | Kolumna 2 | Kolumna3 |");

        Car myCar = new Car();
        myCar.setBrand(Brand.AUDI);
        myCar.setModel("Q4");
        myCar.setPrice(BigDecimal.valueOf(220000));
        myCar.setHorsePower(300);


        Car mySecondCar = new Car();
        mySecondCar.setHorsePower(320);
        mySecondCar.setPrice(BigDecimal.valueOf(520000));
        mySecondCar.setModel("911");
        mySecondCar.setBrand(Brand.PORSCHE);

        database.add(myCar);
        database.add(mySecondCar);

        Scanner scanner = new Scanner(System.in);
        String option;
        do {
            System.out.println("a. Dodaj auto\nb. Zaktualizuj auto\nc. Usun auto\nd. Pokaz auto o id\ne. Pokaz wszystkie auta\nq. Zakoncz");
            option = scanner.nextLine();
            if (ADD_CAR_OPTION.equals(option)) {
                System.out.println("Dodawanie samochodu");
                Car car = carInput();
                database.add(car);
            } else if (UPDATE_CAR_OPTION.equals(option)) {
                System.out.println("Aktualizacja samochodu");
                Car car = carInput();
                database.update(car);
            } else if (DELETE_CAR_OPTION.equals(option)) {
                long id = idInput(scanner);
                database.delete(id);
            } else if (GET_BY_ID_OPTION.equals(option)) {
                long id = idInput(scanner);
                database.get(id)
                        .ifPresent(car -> {
                            printHeader();
                            printCar(car);
                        });
            } else if(GET_ALL_OPTION.equals(option)) {
                List<Car> allCars = database.getAll();
                printHeader();
                for (Car car : allCars) {
                    printCar(car);
                }
            }

        } while (QUIT.equals(option));
        System.out.println("Koniec programu");
    }

    private static void printCar(Car car) {

        System.out.printf("|%4d|%7s|%7s|%5s|%6s|%n",
                car.getId(),
                car.getBrand(),
                car.getModel(),
                car.getHorsePower(),
                car.getPrice());
    }

    private static void printHeader() {
        System.out.println("| id | marka | model | moc | cena |");
    }

    private static long idInput(Scanner scanner) {
        System.out.print("Podaj id: ");
        String stringId = scanner.nextLine();
        return Long.parseLong(stringId);
    }

    private static Car carInput() {
        Scanner scanner = new Scanner(System.in);
        String model = scanner.nextLine();
        String brandString = scanner.nextLine();
        Brand brand = Brand.valueOf(brandString);
        BigDecimal price = new BigDecimal(scanner.nextLine());
        int horsePower = Integer.parseInt(scanner.nextLine());
        Car car = new Car();
        car.setBrand(brand);
        car.setModel(model);
        car.setPrice(price);
        car.setHorsePower(horsePower);
        return car;
    }
}
