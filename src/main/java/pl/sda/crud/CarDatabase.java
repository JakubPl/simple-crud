package pl.sda.crud;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

public class CarDatabase {
    private static final Path DATEBASE_LOCATION = Paths.get("db.csv");
    private List<Car> cars = new ArrayList<>();
    private long nextId;


    public void save() {
        //try-with-resources
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(DATEBASE_LOCATION)) {
            for (Car car : cars) {
                StringBuilder sb = new StringBuilder();
                String line = sb.append(car.getId())
                        .append(";")
                        //..itd. dodajemy poszczegolne pola
                        .append("\n")
                        .toString();
                bufferedWriter.write(line);
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void read() {
        try {
            List<String> lines = Files.readAllLines(DATEBASE_LOCATION);
            //liste linii trzeba splitowac po znaku ";"
            //nastepnie przypisac wynik splita do poszczegolnych pol w Car
            //nastepnie dodac Car do listy "cars"
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }

    }

    public void add(Car car) {
        //TODO: obsluga dodawania auta do listy
        //nadaj nextId dodawanemu samochowodi, zinkrementuj je,
        //pozniej dodaj samochod do listy
        car.setId(nextId);
        cars.add(car);
        nextId++;
    }

    public void update(Car car) {
        ListIterator<Car> carListIterator = cars.listIterator();
        while (carListIterator.hasNext()) {
            Car iter = carListIterator.next();
            if (iter.getId() == car.getId()) {
                carListIterator.set(car);
            }
        }
        //TODO: w liście znajduje auto o identyfikatorze = car.getId() i je podmieniamy

        /*//Nie za dobra praktyka (bo cars.get(i))
        for(int i = 0; i < cars.size(); i++) {
            Car iter = cars.get(i);
            if(iter.getId() == car.getId()) {
                //i == pozycji samochodu do aktualizacji
                cars.set(i, car);
                break;
            }
        }*/

    }

    public void delete(long id) {
        ListIterator<Car> carListIterator = cars.listIterator();
        while (carListIterator.hasNext()) {
            Car iter = carListIterator.next();
            if (iter.getId() == id) {
                carListIterator.remove();
                break;
            }
        }

    }

    public Optional<Car> get(long id) {
        ListIterator<Car> carListIterator = cars.listIterator();
        while (carListIterator.hasNext()) {
            Car iter = carListIterator.next();
            if (iter.getId() == id) {
                return Optional.of(iter);
            }
        }
        return Optional.empty();
    }

    public List<Car> getAll() {
        return cars;
    }
}
